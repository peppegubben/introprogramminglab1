#include <stdio.h> 

int main(void) {
	const int baseInt = 5, heightInt = 5;
	const float baseFloat = baseInt, heightFloat = heightInt;
	
	int   a0 = baseInt   *  heightInt   / 2 ;
	float a1 = baseInt   *  heightInt   / 2 ;
	int   a2 = baseInt   * (heightInt   / 2);
	float a3 = baseFloat * (heightInt   / 2);
	float a4 = baseInt   * (heightFloat / 2);
	float a5 = baseFloat *  heightInt   / 2 ;
	int   a6 = baseFloat *  heightInt   / 2 ;

	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| area  | base  | height | expression                 | result | \n");
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| int   | int   | int    | area = base *  height / 2  | %4d   |  \n", a0 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| float | int   | int    | area = base *  height / 2  | %4g   |  \n", a1 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| int   | int   | int    | area = base * (height / 2) | %4d   |  \n", a2 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| float | float | int    | area = base * (height / 2) | %4g   |  \n", a3 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| float | int   | float  | area = base * (height / 2) | %4g   |  \n", a4 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| float | float | int    | area = base *  height / 2  | %4g   |  \n", a5 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");
	printf("| int   | float | int    | area = base *  height / 2  | %4d   |  \n", a6 );
	printf("+-------+-------+--------+----------------------------+--------+ \n");

	getchar();
	return 0;
}