#include <stdio.h>

/*
 * This program sums up all the first uneven positive integers
 * to the given number.
 */
int main(void) {
	int number;       // A number to test if it's uneven
	int totalAmount;  // The amount of integers that stall be summed. Given by the user.
	int summedAmount; // Keeps track of the summed so far
	int sum = 0;

	/*
	 * Tell the user what the program should do!
	 */
	printf("The program sums the first uneven positive integers 1 + 3 + 5 + ...\n");
	printf("As many as you wish \n\n");

	/*
	 * Read the amount of integers that should be added.
	 */
	printf("How many numbers shall be added? ");
	scanf_s("%d", &totalAmount);
	getchar();

	/*
	 * Calculate the sum
	 */
	summedAmount = 0;
	number = 1; // 0 is negative and should not be taken in. 

	while (summedAmount < totalAmount) { // CHANGED [<=] TO [<]
		if (number % 2 == 1) { // ADDED brackets
			sum += number;
			summedAmount += 1;
		}
		number += 1; // ADDED number counter
	}

	/*
	 * Print the resulting sum
	 */
	printf("\nThe sum of the %d first uneven integers is %d\n", totalAmount, sum);

	getchar();
	return 0;
}