#include <stdio.h> // Enables calls to the printf() - function

int main(void) {
	char myFavoriteCharacter;
	char myFirstName [20];

	printf("Hello world!\n");
	printf("-------------------------\n");

	printf("Please enter your favorite character: ");
	myFavoriteCharacter = getchar();
	getchar();
	printf("Thanks! I now know that your favorite character is %c.\n", myFavoriteCharacter);
	printf("-------------------------\n");
	
	printf("Please enter your first name: ");
	scanf_s("%s", myFirstName, sizeof(myFirstName));
	getchar();
	printf("Thanks! I now know that your first name is %s", myFirstName);
	getchar(); // Makes the program wait for a [ENTER]-press
	return 0;
}